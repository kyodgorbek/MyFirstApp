package com.example.dell.myfirstapp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainScreen extends ActionBarActivity {

    //public final static String EXTRA_MESSAGE = "com.example.dell.myfirstapp.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen_);
    }


    public void sendMessage(View view) {
        Intent startNewActivity = new Intent(this, DisplayMessageActivity.class);
        // EditText theEditText = (EditText) findViewById(R.id.edit_message);
        //String message = theEditText.getText().toString();
        //startNewActivity.putExtra(EXTRA_MESSAGE,message);
        startActivity(startNewActivity);
    }
}